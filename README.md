# My first android project

## Disclaimer
You really better don't look at code. No really! This thing was written by me in a first year in university and it was basically my app done for Google Study Jams(as i was learning Android). So architecture is really bad + lots of things is done in primitive ways(because at that time i wasn't know about most of Android API's and some good frameworks i use now).

So app works great, but you REALLY don't wanna look at code.

## What does it do?

In fact it's app that mirrors your notifications to desktop. It is doing that by Telegram messenger and Telegram API. It just receiving android notifications and than send it to your telegram account by REST api, so you don't miss anything. There are some basic options - you can choose notifications from what apps you wan't to mirror, for example.

<div style="max-width: 20%;max-height: 20%;display: inline-block; align: center;">

<a href="url"><img src="https://raw.githubusercontent.com/schvabodka-man/Screenshots/master/projects/androidnotifier/apps.png" height="400" width="200"  align="left"></a>

<a href="url"><img src="https://raw.githubusercontent.com/schvabodka-man/Screenshots/master/projects/androidnotifier/new.png"  height="400" width="200" align="center"></a>

<a href="url"><img src="https://raw.githubusercontent.com/schvabodka-man/Screenshots/master/projects/androidnotifier/accs.png" height="400" width="200" align="left"></a>

</div>