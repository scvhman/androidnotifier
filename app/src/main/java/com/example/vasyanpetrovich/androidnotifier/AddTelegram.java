package com.example.vasyanpetrovich.androidnotifier;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;


public class AddTelegram extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_telegram);
        if (getIntent().hasExtra("BOT_INFO")) {
            editTelegram();
        } else {
            Log.d("editing", "Nothing to edit, dropped");
        }

    }

    public void telegramInput(View view) { //Метод который тащит токены + каналы с формочек
        EditText tokenInput = (EditText) findViewById(R.id.bottoken);
        String botToken = tokenInput.getText().toString();
        EditText chanelInput = (EditText) findViewById(R.id.chanelname);
        String chanelName = chanelInput.getText().toString();
        SharedPreferences passwords = getSharedPreferences("Logins", Context.MODE_PRIVATE); //это временное
        if (botToken.isEmpty() || chanelName.isEmpty()) {
            Toast emptyFieldsError = Toast.makeText(this, R.string.toast_error_empty_field, Toast.LENGTH_SHORT);
            emptyFieldsError.show();
        } else {
            if (passwords.contains(botToken)) { //временно
                Toast existingToken = Toast.makeText(this, "This token already exists", Toast.LENGTH_SHORT);
                existingToken.show();
            } else {
                telegramSetAccountInfo(botToken, chanelName);
            }
        }
    }

    public void telegramSetAccountInfo(String token, String chanel) { //Вот тут вот в шейред преференс. Хуй знает зачем так сделал. Стоит перепилить на SQLITE или пассворд сторейдж.
        TelegramConnection newConnection = new TelegramConnection();
        newConnection.setToken(token);
        newConnection.setChanel(chanel);
        SharedPreferences passwords = getSharedPreferences("Logins", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = passwords.edit();
        editor.putString(newConnection.getToken(), newConnection.getChanel());
        editor.apply();
        Intent mainIntent = new Intent(this, MainScreen.class);
        startActivity(mainIntent);
    }

    public void editTelegram() { //редактирование от бати
        Bundle receiveStuffFromIntent = getIntent().getBundleExtra("BOT_INFO");
        final String token = receiveStuffFromIntent.get("TELEGRAM_TOKEN").toString(); //Всё это файналами т.к нужно для редактирования из иннер класса
        String chanel = receiveStuffFromIntent.get("TELEGRAM_CHANEL").toString();
        final EditText botField = (EditText) findViewById(R.id.bottoken);
        final EditText chanelField = (EditText) findViewById(R.id.chanelname);
        final Intent returnToMain = new Intent(this, MainScreen.class);
        botField.setText(token);
        chanelField.setText(chanel);
        Button submit = (Button) findViewById(R.id.submitbutton);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences passwords = getSharedPreferences("Logins", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = passwords.edit();
                editor.remove(token);
                editor.putString(botField.getText().toString(), chanelField.getText().toString());
                editor.apply();
                startActivity(returnToMain);
                Log.d("editing", "Successfully edited");
            }
        });
    }
}

