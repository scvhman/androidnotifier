package com.example.vasyanpetrovich.androidnotifier;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

public class SettingsMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_menu);
        SharedPreferences notificationsTitle = getSharedPreferences("Title", Context.MODE_PRIVATE);
        CheckBox checker = (CheckBox) findViewById(R.id.title_checker);
        if (notificationsTitle.contains("Title")) {
            checker.setChecked(true);
        } else {
            checker.setChecked(false);
        }
    }

    public void setNotificationsFromWhatApps(View textView) {
        Intent changeActivityToSet = new Intent(this, SettingsChooseNotificationsFromApps.class);
        startActivity(changeActivityToSet);
    }

    public void checkTitle(View view) { //это класс для работы с чекбоксом в UI
        CheckBox checker = (CheckBox) findViewById(R.id.title_checker);
        SharedPreferences notificationsTitle = getSharedPreferences("Title", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorNotificationsTitle = notificationsTitle.edit();
        if (checker.isChecked()) {
            editorNotificationsTitle.putString("Title", "true");
            editorNotificationsTitle.apply();
        } else {
            editorNotificationsTitle.remove("Title");
            editorNotificationsTitle.apply();
        }
    }
}
