package com.example.vasyanpetrovich.androidnotifier;

import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class NotificationsListener extends NotificationListenerService {

    @Override
    public void onNotificationPosted(StatusBarNotification notification) {
        Bundle newNotifications = notification.getNotification().extras;
        SharedPreferences notificationsTitle = getSharedPreferences("Title", Context.MODE_PRIVATE);
        if (notificationsTitle.contains("Title")) {
            String getNotificationText = newNotifications.getCharSequence(Notification.EXTRA_TITLE).toString() + System.lineSeparator() + newNotifications.getCharSequence(Notification.EXTRA_TEXT).toString();
            getAppsContains(getNotificationText, notification.getPackageName());
        } else {
            String getNotificationText = newNotifications.getCharSequence(Notification.EXTRA_TEXT).toString();
            getAppsContains(getNotificationText, notification.getPackageName());
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification notification) {
        Log.d("notification", "notification from app " + notification.getPackageName() + "is removed from status bar");
    }

    public void getAppsContains(String textToSend, String packageName) {
        SharedPreferences appsGoto = getSharedPreferences("AppsTelegram", Context.MODE_PRIVATE);
        TelegramSendNotification sendNotificationToTelegram = new TelegramSendNotification();
        if (appsGoto.contains(packageName)) {
            sendNotificationToTelegram.execute(textToSend);
            Log.d("notification", "notification from app " + packageName + "is send");
        } else {
            Log.d("notification", "notification from app " + packageName + "is dropped");
        }
    }

    public class TelegramSendNotification extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            SharedPreferences telegramSendTo = getSharedPreferences("Logins", Context.MODE_PRIVATE);
            Map<String, ?> telegramSendMap = telegramSendTo.getAll();
            //Аналогично менюшке тащит из мапы
            for (Map.Entry<String, ?> entry : telegramSendMap.entrySet()) {
                OkHttpClient telegramClient = new OkHttpClient();
                String sendToken = entry.getKey();
                String sendChanel = (String) entry.getValue();
                String notification = params[0];
                String telegramLink = "https://api.telegram.org/bot" + sendToken + "/sendMessage?chat_id=" + sendChanel + "&text=" + notification;
                Request telegramSendNotification = new Request.Builder().url(telegramLink).build();
                try {
                    Response telegramConnect = telegramClient.newCall(telegramSendNotification).execute();
                    telegramConnect.body().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
