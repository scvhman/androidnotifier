package com.example.vasyanpetrovich.androidnotifier;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Set;

public class SettingsChooseNotificationsFromApps extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener { //имплемент для того чтобы не плодить лишние классы

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_choose_notifications_from_apps);
        createAppsMenu();
    }

    public void createAppsMenu() { //Класс для запиливания менюшки с софтом
        PackageManager getInstalledApps = getPackageManager();
        List packagesList = getInstalledApps.getInstalledPackages(0); //У этого метода очень странные флаги
        LinearLayout appsMenuLayout = (LinearLayout) findViewById(R.id.apps_menu_scroller);
        SharedPreferences telegramAppsGoto = getSharedPreferences("AppsTelegram", Context.MODE_PRIVATE);
        int textViewsId = 0; // ну хз чё там. Просто вот так назначил айдишки
        int checkBoxesId = 1;
        for (int i = 0; i < packagesList.size(); i++) {
            textViewsId = textViewsId + 2;
            checkBoxesId = checkBoxesId + 2;
            LinearLayout layoutForText = new LinearLayout(this);
            PackageInfo packageInfo = (PackageInfo) packagesList.get(i); //Достаю инфу о пакетах
            ImageView iconPackage = new ImageView(this);
            CheckBox checkApplication = new CheckBox(this);
            TextView textPackage = new TextView(this);
            checkApplication.setId(checkBoxesId);
            try { //Трай кетч на случай если что то пойдёт не так
                iconPackage.setImageDrawable(getInstalledApps.getApplicationIcon(packageInfo.packageName));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                Log.e("error", "NameNotFoundException in createAppsMenu()");
            }
            LinearLayout.LayoutParams iconSize = new LinearLayout.LayoutParams(72, 72);
            iconPackage.setLayoutParams(iconSize);
            textPackage.setId(textViewsId);
            textPackage.setText(packageInfo.packageName);
            textPackage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            if (telegramAppsGoto.contains(getIntent().getStringExtra("TELEGRAM_NAME"))) { //проверка на наличие в шейред преференсе такого ключа
                checkApplication.setChecked(true);
            } else {
                checkApplication.setChecked(false);
            }
            checkApplication.setOnCheckedChangeListener(this); // это после сетчекеда, потмоу что это очевидно вызывает конфликт
            layoutForText.addView(iconPackage);
            layoutForText.addView(textPackage);
            layoutForText.addView(checkApplication);
            appsMenuLayout.addView(layoutForText);
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences telegramAppsGoto = getSharedPreferences("AppsTelegram", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorTelegramApps = telegramAppsGoto.edit();
        int textViewId = buttonView.getId() - 1;
        TextView appName = (TextView) findViewById(textViewId);
        String name = getIntent().getStringExtra("TELEGRAM_NAME");
        if (isChecked) {
            Set<String> getApps = telegramAppsGoto.getStringSet(name, null);
            if (getApps == null) {
                Log.d("pe", "zda");
            }
            editorTelegramApps.putString(name, appName.getText().toString());
            editorTelegramApps.apply();
        } else {
            editorTelegramApps.remove(appName.getText().toString());
            editorTelegramApps.apply();
        }
    }
}
