package com.example.vasyanpetrovich.androidnotifier;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        makeTelegramMenuGovnokod();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public void newAccountMenu(MenuItem menuItem) {
        Intent setupNewAccount = new Intent(this, AddTelegram.class);
        startActivity(setupNewAccount);
    }

    public void showSettingsMenu(MenuItem menuItem) {
        Intent getToSettings = new Intent(this, SettingsMenu.class);
        startActivity(getToSettings);
    }

    public void setNotificationsFromWhatApps(String name) { //переход на выбор прог
        Intent changeActivityToSet = new Intent(this, SettingsChooseNotificationsFromApps.class);
        changeActivityToSet.putExtra("TELEGRAM_NAME", name);
        startActivity(changeActivityToSet);
    }

    public void makeTelegramMenuGovnokod() {
        //Метод который запиливает говноменюшку с акками из телеграма на мейн активити
        SharedPreferences passwords = getSharedPreferences("Logins", Context.MODE_PRIVATE);
        Map<String, ?> telegramAccounts = passwords.getAll();
        LinearLayout scrollerLayout = (LinearLayout) findViewById(R.id.layoutscroller);
        //Вытаскивает из мапы значения ключа-валуе(токен-канал) и выводит их на экран. Плюс иконочку добавляет
        for (Map.Entry<String, ?> entry : telegramAccounts.entrySet()) {
            final String token = entry.getKey();
            final String chanel = (String) entry.getValue();
            TextView showChanel = new TextView(this);
            showChanel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            LinearLayout newAccount = new LinearLayout(this);
            showChanel.setText("Bot token:" + token + "\n" + "Chanel:" + chanel);
            showChanel.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() { //ну как то так
                @Override
                public void onCreateContextMenu(ContextMenu menu, final View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add(0, 1, 0, R.string.menu_edit).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            telegramEdit(token, chanel);
                            return true;
                        }
                    });
                    menu.add(0, 2, 1, R.string.menu_delete).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            telegramDelete(token, v);
                            return true;
                        }
                    });
                    menu.add(0, 3, 2, R.string.menu_apps_choose).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            setNotificationsFromWhatApps(token);
                            return true;
                        }
                    });
                }
            });
            newAccount.addView(showChanel);
            scrollerLayout.addView(newAccount);
        }
    }

    public void telegramDelete(String token, View bot) { //Клас для выпиливания телеграмоакков
        SharedPreferences passwords = getSharedPreferences("Logins", Context.MODE_PRIVATE);
        SharedPreferences.Editor deleteFromTelegram = passwords.edit();
        deleteFromTelegram.remove(token);
        deleteFromTelegram.apply();
        bot.setVisibility(View.GONE);
        Log.d("editing", "Telegram account deleted");
    }

    public void telegramEdit(String token, String chanel) { //клас для редактирование телеграммоаккаунтов
        Bundle telegramInfo = new Bundle();
        telegramInfo.putString("TELEGRAM_TOKEN", token);
        telegramInfo.putString("TELEGRAM_CHANEL", chanel);
        Intent telegramEditIntent = new Intent(this, AddTelegram.class);
        telegramEditIntent.putExtra("BOT_INFO", telegramInfo);
        startActivity(telegramEditIntent);
        Log.d("editing", "Now editing");
    }
}